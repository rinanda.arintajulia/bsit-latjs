//const score = [80, 70, 20, 100]

//const math = score[0]
//const science = score[1]
//const phy = score[2]
//const indonesia = score[3]

//const [match, sci, phy, ind] = score
//console.log(math)

const obj1 = {
    firstName: 'Rinanda',
    lastName: 'Julia',
    fullName: function(){
        return `${this.firstName} ${this.lastName}`
    }
}

// const firstName = obj1.firstName
// const lastName = obj1.lastName
// const fullName = obj1.fullName()

const {firstName: name, lastName: last} = obj1
console.log(last)
