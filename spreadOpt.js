const arr1 = [1,2,3]
const arr2 = [4,5,6]
const arr3 = [...arr1, ...arr2]

//console.log(arr3)

const obj1 = {
    firstName: 'Rinanda',
    lastName: 'Julia',
    fullName: function(){
        return `${this.firstName} ${this.lastName}`
    }
}

const obj2 = {
    age: 22,
    birthDate: '17/07/2000'
}
const obj3 = {...obj1, ...obj2}

console.log(obj3.fullName())

